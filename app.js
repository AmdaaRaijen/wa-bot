import pkg from "whatsapp-web.js";
const { Client, LocalAuth, MessageMedia, Buttons } = pkg;
import qrcode from "qrcode-terminal";

const client = new Client({
  authStrategy: new LocalAuth(),
});

client.on("qr", (qr) => {
  console.log("QR RECEIVED", qr);
  qrcode.generate(qr, { small: true });
});

client.on("ready", () => {
  console.log("Client is ready!");
});

client.on("message", async (msg) => {
  const imgExample = MessageMedia.fromFilePath("./image.png");
  if (
    (msg.body === "!stiker" || msg.body === "!sticker") &&
    msg.type === "image"
  ) {
    let media;
    try {
      media = await msg.downloadMedia();
    } catch (error) {
      console.log(error);
      msg.reply("Proses Mengunduh gambar gagal");
    }
    client.sendMessage(msg.from, "⏳ Tunggu sebentar...");
    client.sendMessage(
      msg.from,
      "Jaga kesopanan yaa... \nkirim gambar yang baik baik 🤨📸"
    );
    client.sendMessage(msg.from, media, {
      sendMediaAsSticker: true,
      stickerAuthor: "BOT-WA-STICKER/BT",
      stickerName: "Name",
    });
  } else {
    client.sendMessage(
      msg.from,
      "Kirim Sebuah gambar dengan caption \n!stiker"
    );
    client.sendMessage(
      msg.from,
      "Jaga kesopanan yaa... \nkirim gambar yang baik baik 🤨📸"
    );
    client.sendMessage(msg.from, "contoh:");
    client.sendMessage(msg.from, imgExample, {
      caption: "!stiker",
    });
  }
});

client.initialize();
